# GitLab CI/CD Training Repository

[![build status](https://gitlab.com/drumrigj/gitlab-ci-training/badges/master/build.svg)](https://gitlab.com/drumrigj/gitlab-ci-training/commits/master)

This code sample is part of a CI/CD training session. It uses the Lumen PHP framework but no PHP knowledge is required. 

You can view the slides for this training [here](https://docs.google.com/presentation/d/1aV97b4eubIwV6j2LNhTuUYf_p4qJCHT5fmsgQbCSSyg/edit?usp=sharing)

If you'd just like to tinker with the code, please see the `Nuts and Bolts` section of this README.

## Getting Started

This project creates an API that displays Pokémon information like a Pokédex. It's designed to work with GitLab, so if you don't have an account you can create one [here](https://gitlab.com/users/sign_in).

This project will also automatically deploy to Heroku when configured. To accomplish this, you'll need to have a Heroku account as well as Heroku's CLI utility installed.

## Layout

There are several git branches with changes that will turn this project into something that gets tested and automatically deployed to Heroku. To complete the project, you simply need to merge all of the branches into the `master` branch.

## Routes

Routes tell the application what to do when we get a request. Here's an overview of what's happening under the hood.

|Method   |/endpoint       |Description                       |
|---------|----------------|----------------------------------|
|GET      |/pokemon        |Get a list of Pokémon             |
|POST     |/pokemon        |Add a Pokémon to the Pokédex      |
|GET      |/pokemon/{id}   |Get information about a Pokémon   |
|PUT      |/pokemon/{id}   |Update a Pokémon's entry          |
|DELETE   |/pokemon/{id}   |Delete a Pokémon                  |

## Contributing

If you'd like to contribute to this project, please open an issue or a merge request. As the content here is specific to the training session, please note that some requests may be denied if they're not relevant to the aucience.

To work with Lumen, please see the [Lumen Installation Guide](https://lumen.laravel.com/docs/5.4/installation) to get started.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Nuts and Bolts

This section assumes that you have some familiarity with PHP and the Lumen framework. If you don't, please check out the documentation for more information.

### Configuration and Setup

Copy `.env.example` to `.env` before getting started. You will need to add a 32 character `APP_KEY` -- this can be any random alphanumeric characters.

You will also need to run `composer install` from within the top level project directory to install dependencies.

The app is served from the `public` folder. If you're working locally you can use PHP's built in web server by typing `php -S localhost:9001 -t public/` from within the project's top level directory.

### Database

This project uses the Eloquent ORM which allows you to choose from a variety of databases. In this example, we use `psql` for Postgres, but you can optionally use MySQL or another DB of your choice.

Eloquent is disabled by default, but is currently enabled in this app. You can toggle this on or off in the `bootstrap/app.php` file by commenting or uncommenting the `$app->withEloquent()` call.

#### Migrations

In order to create the proper data tables, you will need to run migrations. To do this, run `php artisan migrate` inside of the top level project directory. Please note that the tables will be empty unless you also run `php artisan db:seed` to populate them with data.

You can adjust which data is seeded in `database/seeds/PokemonSeeder.php`. If you add new seed files, you will need to add them to the `database/seeds/DatabaseSeeder.php` file.

### Exceptions

The exception handler has been modified to display errors in JSON. You can play with it in `app/Exceptions/Handler.php`.

### Routing

All routes are located in `routes/web.php`.

### Logging

Lumen logs to `storage/logs/lumen.log`.

### Tests

All tests are located in the `tests` directory and use PHPUnit. Simply run `vendor/bin/phpunit` to run tests.
